package Modal;

/**
 * Created by Dell on 12/4/2017.
 */

public class User {

    String name;
    String email;
    String UserId;
    String pass;
    String mobile;
    String city;

    public User() {
    }

    public User(String name, String email, String userId, String pass, String mobile, String city) {
        this.name = name;
        this.email = email;
        UserId = userId;
        this.pass = pass;
        this.mobile = mobile;
        this.city = city;
    }

    public String getMobile() {
        return mobile;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

}
