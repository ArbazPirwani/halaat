package Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.halaat.halaat.R;

import java.util.ArrayList;

import Modal.Contact;

/**
 * Created by Dell on 12/25/2017.
 */

public class ContactAdapter extends ArrayAdapter {

    ArrayList<Contact> contacts;
    ArrayList<Integer> index;

    public ContactAdapter(@NonNull Context context, int resource, ArrayList<Contact> contacts, ArrayList<Integer> index) {
        super(context, 0, contacts);
        this.contacts = contacts;
        this.index = index;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.contact_item, parent, false);
        }
        TextView name = convertView.findViewById(R.id.name);
        name.setText(contacts.get(position).getName());

        TextView number = convertView.findViewById(R.id.phone);
        number.setText(contacts.get(position).getPhone());


        CheckBox checkBox = convertView.findViewById(R.id.check);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    index.add(position);
                } else {
                    index.remove(position);
                }
            }
        });

        return convertView;
    }
}
