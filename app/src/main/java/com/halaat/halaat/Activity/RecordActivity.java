package com.halaat.halaat.Activity;

import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.halaat.halaat.R;

import java.io.File;
import java.io.IOException;

public class RecordActivity extends AppCompatActivity {
    String path;
    MediaRecorder recorder ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);


    }

    public void record(View view) {
        recorder = new MediaRecorder();
        String status = Environment.getExternalStorageState();
        if (status.equals("mounted")) {
            path = Environment.getExternalStorageDirectory().getPath();
        }
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_2_TS);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(path);
        try {
            recorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        recorder.start();
    }

    public void stop(View view) {
        recorder.stop();
        recorder.release();
    }
}
