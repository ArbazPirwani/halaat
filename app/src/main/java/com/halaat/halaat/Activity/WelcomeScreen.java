package com.halaat.halaat.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.halaat.halaat.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class WelcomeScreen extends AppCompatActivity implements View.OnClickListener {
    DatabaseReference databaseReference;
    Button halaat,ICE,fixit,audio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        linkXML();
        setListener();
        work();
    }

    public void linkXML() {
        halaat = findViewById(R.id.halaat);
        ICE = findViewById(R.id.ice_button);
        ICE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(WelcomeScreen.this, ICE.class);
                startActivity(intent);
            }
        });
        fixit = findViewById(R.id.fixit_photo);
        audio = findViewById(R.id.audio);
    }

    public void setListener() {
        halaat.setOnClickListener(this);
        fixit.setOnClickListener(this);
    }

    public void work() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fixit_photo) {
            Intent intent = new Intent(WelcomeScreen.this, FixitPhoto.class);
            startActivity(intent);
        }

        if (view.getId() == R.id.halaat) {
            Intent intent = new Intent(WelcomeScreen.this, LocationActivity.class);
            startActivity(intent);
        }
    }
}
